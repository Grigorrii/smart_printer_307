from arduino_reader import wait_uid
from client import printer_pulling

if __name__=='__main__':
    try:
        printer_pulling()
        wait_uid(COM='/dev/ttyACM0')
    except:
        print('Ошибка в работе программы, перезапуск')
        wait_uid(COM='/dev/ttyACM0')