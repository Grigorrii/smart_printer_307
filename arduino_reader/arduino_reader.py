import time
import serial
import client
import serial.tools.list_ports

def check_connections(COM):
    myports = [tuple(p) for p in list(serial.tools.list_ports.comports())]
    for i in myports:
        if COM in i[0]:
            print('Порт найден')
            return COM
    print('Порт не найден')
    for i in myports:
        if 'Arduino Uno' in i[1]:
            print('Удалось найти Ардуино Уно на порту', i[0])
            return i[0]
    time.sleep(2)
    return None

def connect_arduino(COM, reconnect=False):
    try:
        COM = check_connections(COM)
        if COM != None:
            ser = serial.Serial(COM, 9600, timeout=1)
            ser.flush()
            if reconnect is False:
                print('Подключение удалось')
                print('Port:', COM)
            else:
                print('Связь с Arduino восстановлена')
            return ser
    except:
        print('Не удалось подключиться к Arduino Uno')
        print('Port:', COM)
        time.sleep(2)
        connect_arduino(COM)

def read_uid(ser):
    try:
        uid = ser.readline().decode('utf-8').rstrip()
        if uid != '':
            return uid
        else:
            return 10
    except:
        print('Потеряна связь с Arduino')
        return 13

def wait_uid(COM='COM8'):
    ser = connect_arduino(COM)
    while True:
        code = read_uid(ser)
        if code == 13:
            ser = connect_arduino(COM, reconnect=True)
        elif code == 10:
            continue
        else:
            print(str(code)[1:])
            client.send_uid(str(code)[1:])


