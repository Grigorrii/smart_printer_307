import datetime
import time
import requests
import codecs
import os
import threading
import cups
import logging
import shutil


printer_name = 'penis410'
host = 'http://localhost:5000'
conn = cups.Connection()
global_state = None
logging.basicConfig(level=logging.INFO, filename="printer.log", filemode="a")


def printer_pulling():
    def thread_pulling():
        global global_state
        print('Statr pulling printer')
        start_printing_time = time.time()
        while True:
            current_state = conn.getPrinters()[printer_name]['printer-state']
            if global_state != None and current_state != global_state:
                if global_state == 3 and current_state == 4:
                    start_printing_time = time.time()
                    print('Start printing')
                elif global_state == 4 and current_state == 3:
                    end_printing_time = time.time()
                    print('End printing')
                    printing_duration = round(
                        end_printing_time-start_printing_time, 2)
                    print('Printing duration:', printing_duration)
                # print('State change. Now:', current_state)
            global_state = current_state
            time.sleep(0.05)
    thread_p = threading.Thread(target=thread_pulling)
    thread_p.start()

# TODO return job_duration


def check_job_status(job_id: int):
    start_time = time.time()
    print(conn.getJobAttributes(job_id)['job-state'])
    global_job_state = conn.getJobAttributes(job_id)['job-state-reasons']
    print(global_job_state)
    print()
    while time.time()-start_time <= 15:
        current_job_state = conn.getJobAttributes(job_id)['job-state-reasons']
        if current_job_state != global_job_state:
            print(current_job_state)
            print(conn.getJobAttributes(job_id)['job-state'])
            global_job_state = current_job_state

        # if current_job_state == 'processing-to-stop-point':
        #     print('end_fuck')
        #     return
        time.sleep(0.1)
    print('end_uck')


def send_uid(uid: str):
    def thread_sending():
        try:
            data = requests.get(host+'/printer/print/'+uid)
            # print(data.content)
        except:
            print('No connection to', host)
            logging.error('Cannot connect to server')
            return
        if data.status_code == 404:
            logging.error('No files on server')
            print('Файлы на сервере не найдены')
            return
        if data.status_code != 200:
            print('Error', data.status_code)
            logging.error('STATUS CODE: {}'.format(data.status_code))
            return
        if data.content.decode('utf-8') == 'null':
            print('Stop Thread')
            return
        data = eval(data.content.decode('utf-8'))
        filenames_list = []
        print('len:', len(data))
        try:
            shutil.rmtree('pdf_files')
        except:
            pass
        for i in range(len(data)):
            data_bytes = data[i][0]
            headers = data[i][1]
            print(headers['name'], headers['id'])
            # print(data_bytes)
            # print(type(data_bytes), '\n\n\n\n')
            files_dir = 'pdf_files/' + uid
            filename = files_dir+'/' + headers['name'] + '.pdf'
            filenames_list.append(filename)
            os.makedirs(os.path.dirname(filename), exist_ok=True)
            with codecs.open(filename, 'wb') as f:
                f.write(data_bytes)
                f.close()

        print('Файлы в папке', uid, 'записаны')
        print('Cтарт печати')
        # print(conn.getPrinters())
        print(printer_name, filenames_list, uid)
        job_id = conn.printFiles(printer_name, filenames_list, uid, {
                                 'fit-to-page': 'True'})
        # check_job_status(job_id=job_id)
        time.sleep(2)

        # print(conn.getJobAttributes(job_id)["job-state"])
        return
    thread = threading.Thread(target=thread_sending)
    thread.start()
