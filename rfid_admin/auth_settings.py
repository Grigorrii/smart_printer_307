import tkinter
import tkinter.messagebox
import time
import customtkinter
import serial
import serial.tools.list_ports
import sys
import threading
import requests

customtkinter.set_appearance_mode("Dark")  # Modes: "System" (standard), "Dark", "Light"
customtkinter.set_default_color_theme("blue")  # Themes: "blue" (standard), "green", "dark-blue"
customtkinter.set_widget_scaling(1.2)

class App(customtkinter.CTk):
    def __init__(self):
        self.arduino_thread = None
        self.port = None
        self.uid = None
        self.wait_uid_bool = True
        self.token = None
        self.server_adress = 'http://127.0.0.1:5000/'
        super().__init__()

        # configure window
        self.title("OLEG PRINTER RFID ADMIN")
        self.geometry(f"{1100}x{580}")

        # configure grid layout (4x4)
        self.grid_columnconfigure(1, weight=1)
        self.grid_columnconfigure((2, 3), weight=0)
        self.grid_rowconfigure((0, 1, 2, 3), weight=1)

        # create sidebar frame with widgets
        self.sidebar_frame = customtkinter.CTkFrame(self, width=140, corner_radius=0)
        self.sidebar_frame.grid(row=0, column=0, rowspan=4, sticky="nsew")
        self.sidebar_frame.grid_rowconfigure(4, weight=1)
        self.logo_label = customtkinter.CTkLabel(self.sidebar_frame, text="OLEG PRINTER", font=customtkinter.CTkFont(size=20, weight="bold"))
        self.logo_label.grid(row=0, column=0, padx=20, pady=(20, 10))
        self.appearance_mode_label = customtkinter.CTkLabel(self.sidebar_frame, text="Appearance Mode:", anchor="w")
        self.appearance_mode_label.grid(row=5, column=0, padx=20, pady=(10, 0))
        self.appearance_mode_optionemenu = customtkinter.CTkOptionMenu(self.sidebar_frame, values=["Light", "Dark", "System"],
                                                                       command=self.change_appearance_mode_event)
        self.appearance_mode_optionemenu.grid(row=6, column=0, padx=20, pady=(10, 10))
        self.scaling_label = customtkinter.CTkLabel(self.sidebar_frame, text="UI Scaling:", anchor="w")
        self.scaling_label.grid(row=7, column=0, padx=20, pady=(10, 0))
        self.scaling_optionemenu = customtkinter.CTkOptionMenu(self.sidebar_frame, values=["80%", "90%", "100%", "110%", "120%"],
                                                               command=self.change_scaling_event)
        self.scaling_optionemenu.grid(row=8, column=0, padx=20, pady=(10, 20))

        self.login_entry = customtkinter.CTkEntry(self.sidebar_frame, placeholder_text="Логин")
        self.login_entry.grid(row=2, column=0)

        self.password_entry = customtkinter.CTkEntry(self.sidebar_frame, placeholder_text="Пароль", show='*')
        self.password_entry.grid(row=2, column=0, pady=(70, 0))

        self.login_button = customtkinter.CTkButton(self.sidebar_frame, text='Авторизоваться', command=self.login_admin)
        self.login_button.grid(row=2, column=0, pady=(140, 0))

        self.session_status_label = customtkinter.CTkLabel(self.sidebar_frame, text='Session not active', text_color='red', font=customtkinter.CTkFont(size=14, weight="bold"))
        self.session_status_label.grid(row=1, column=0)

        # create main window
        self.rfid_status_label = customtkinter.CTkLabel(self, text='RFID reader not active', text_color='red', font=customtkinter.CTkFont(size=20, weight="bold"))
        self.rfid_status_label.grid(row=0, column=1)

        self.rfid_name_label = customtkinter.CTkLabel(self, text='Считанный UID:', font=customtkinter.CTkFont(size=20, weight="bold"))
        self.rfid_name_label.grid(row=1, column=1)

        self.rfid_label = customtkinter.CTkLabel(self, text='', font=customtkinter.CTkFont(size=26, weight="bold"))
        self.rfid_label.grid(row=1, column=1, pady=(100,0))

        # user list frame

        self.user_list_frame = customtkinter.CTkFrame(self, width=400, height=400, corner_radius=10)
        self.user_list_frame.grid(row=2, column=1, rowspan=2, sticky="nsew", padx=(30,30), pady=40)
        self.user_list_frame.grid_rowconfigure(6, weight=1)



        self.scrollbar = customtkinter.CTkScrollbar(self.user_list_frame)
        self.scrollbar.grid(row=0, column=1, sticky="ns")

        # create settings frame
        self.settings_frame = customtkinter.CTkFrame(self, width=300, corner_radius=10)
        self.settings_frame.grid(row=0, column=3, rowspan=4, sticky="nsew", padx=(10,30), pady=40)
        self.settings_frame.grid_rowconfigure(6, weight=1)

        self.settings_label = customtkinter.CTkLabel(self.settings_frame, text="Настройки:", font=customtkinter.CTkFont(size=20, weight="bold"))
        self.settings_label.grid(row=0, column=0, padx=40, pady=10)

        self.arduino_port_label = customtkinter.CTkLabel(self.settings_frame, text='Arduino Port: ', anchor='w')
        self.arduino_port_label.grid(row=1, column=0, padx=20, pady=(5, 0))

        self.arduino_port_entry = customtkinter.CTkEntry(self.settings_frame, placeholder_text="/dev/ttyACM0")
        self.arduino_port_entry.grid(row=2, column=0, pady=(5, 0))

        self.available_arduino_ports_label = customtkinter.CTkLabel(self.settings_frame, text='Доступные порты: ', anchor='w')
        self.available_arduino_ports_label.grid(row=3, column=0, padx=20, pady=(10, 70))

        self.available_arduino_ports_optionemenu = customtkinter.CTkOptionMenu(self.settings_frame,
                                                              command=self.set_arduino_port)
        self.available_arduino_ports_optionemenu.grid(row=3, column=0, padx=20)

        self.reload_available_arduino_ports_button = customtkinter.CTkButton(self.settings_frame, text='Обновить порты', command=self.reload_arduino_ports)
        self.reload_available_arduino_ports_button.grid(row=4, column=0, pady=(0, 20))

        self.connect_arduino_button = customtkinter.CTkButton(self.settings_frame, text='Подключиться', command=self.connect_arduino)
        self.connect_arduino_button.grid(row=7, column=0, pady=(0, 20))




        # set default values
        self.appearance_mode_optionemenu.set("Dark")
        self.scaling_optionemenu.set("120%")
        self.reload_arduino_ports()
        self.set_system_label()

    def login_admin(self):
        login = self.login_entry.get()
        password = self.password_entry.get()
        # print(password)

        payload = {'email': login,
                   'password': password
                   }

        print(self.server_adress+'users/login')
        token = requests.post(url=self.server_adress+'users/login', json=payload)
        print(token.status_code)
        response_data = token.json()
        if token.status_code == 200 and response_data['admin']:
            self.token = response_data['access_token']
            self.session_status_label.configure(text_color='green', text='Сессия активна')
            print('OK')
        elif token.status_code == 200 and not response_data['admin']:
            self.session_status_label.configure(text_color='red', text='Вы не являетесь администратором')
            print('Вы не являетесь администратором')
            self.token = None
        elif token.status_code == 404:
            self.session_status_label.configure(text_color='red', text='Неверный логин или пароль')
            self.token = None
            print('Неверный логин или пароль')
        else:
            print('Ошибка авторизации')

    def reload_arduino_ports(self):
        self.myports = [tuple(p) for p in list(serial.tools.list_ports.comports())]
        ard_ports = []
        for i in self.myports:
            ard_ports.append(i[0])
        self.available_arduino_ports_optionemenu.configure(values=ard_ports)
        # print(ard_ports)


    def set_arduino_port(self, port: str):
        self.arduino_port_entry.delete(0, 'end')
        self.arduino_port_entry.insert(0, port)


    def set_system_label(self):
        # print(sys.platform)
        if sys.platform == "linux":
            self.arduino_port_entry.configure(placeholder_text='/dev/ttyACM0')
        elif sys.platform == "win32":
            self.arduino_port_entry.configure(placeholder_text='COM8')

    def change_appearance_mode_event(self, new_appearance_mode: str):
        customtkinter.set_appearance_mode(new_appearance_mode)

    def change_scaling_event(self, new_scaling: str):
        new_scaling_float = int(new_scaling.replace("%", "")) / 100
        customtkinter.set_widget_scaling(new_scaling_float)

    def connect_arduino(self):
        self.port = self.arduino_port_entry.get()
        # try:
        self.ser = serial.Serial(self.port, 9600, timeout=1)
        self.ser.flush()
        self.rfid_status_label.configure(text_color='green', text='Связь с Arduino установлена')
        for i in self.myports:
            if i[0] == self.port:
                self.rfid_status_label.configure(text_color='green', text='Связь с {} установлена'.format(i[1]))
        if self.arduino_thread == None:
            self.arduino_thread = threading.Thread(target=self.wait_uid)
            self.arduino_thread.start()
        elif self.arduino_thread.is_alive():
            self.wait_uid_bool = False
            while self.arduino_thread.is_alive():
                continue
            self.wait_uid_bool = True
            self.arduino_thread = threading.Thread(target=self.wait_uid)
            self.arduino_thread.start()
        elif self.wait_uid_bool is False and not self.arduino_thread.is_alive():
            self.wait_uid_bool = True
            self.arduino_thread = threading.Thread(target=self.wait_uid)
            self.arduino_thread.start()
        # except:
        #     self.rfid_status_label.configure(text_color='red', text='Не удалось подключиться')

    def read_uid(self):
        try:
            uid = self.ser.readline().decode('utf-8').rstrip()
            if uid != '':
                return uid
            else:
                return 10
        except:
            # print('Потеряна связь с Arduino')
            return 13

    def kill_thread(self):
        self.wait_uid_bool = False

    def wait_uid(self):
        print('thread started')
        while self.wait_uid_bool:
            code = self.read_uid()
            if code == 13:
                self.rfid_status_label.configure(text='Потеряна связь с устройством', text_color='red')
                self.kill_thread()
                self.reload_arduino_ports()
            elif code == 10:
                continue
            else:
                print(str(code)[1:])
                self.uid = code[1:]
                self.rfid_label.configure(text=self.uid)
                print(self.server_adress+'admin/users/finally_connect?rf_id='+self.uid.replace(' ', ''))
                response_data = requests.get(url=self.server_adress+'admin/users/finally_connect?rf_id='+self.uid.replace(' ', ''), headers={'Authorization': 'Bearer '+self.token})
                print(response_data.status_code)
                # 'http://192.168.3.40/admin/users/finally_connect?rf_id=123'


if __name__ == "__main__":
    app = App()
    app.mainloop()
    app.kill_thread()
