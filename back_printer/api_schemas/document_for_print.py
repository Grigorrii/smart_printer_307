from pydantic import BaseModel, root_validator
from typing import List, Optional
from datetime import datetime
from bson.objectid import ObjectId


class SendForPrintSchema(BaseModel):
    name: str
    id: str 
    media_type: Optional[str] = "application/pdf"
