from pydantic import BaseModel, root_validator
from typing import List, Optional
from datetime import datetime
from bson.objectid import ObjectId
from models.document_model import DocumntMongoDocument


class StatPeriodOneUsers(BaseModel):
    id: Optional[str] = None
    name: str
    size: int
    time_print: datetime

    @root_validator(pre=True)
    def fix_id(cls, values):
        try:
            values['id'] = str(values["_id"])
            return values
        except:
            return values


class StatPeriodAllUsers(BaseModel):
    id: Optional[str] = None
    name: Optional[str] = None
    size_pages: Optional[int] = None
    count_document: Optional[int] = None
    dt_last_print: Optional[datetime] = None

    @root_validator(pre=True)
    def fix_id(cls, values):
        try:
            values['id'] = str(values["_id"])
            return values
        except:
            return values



class ResultOneUser(BaseModel):
    info: StatPeriodAllUsers
    table: List[DocumntMongoDocument]

