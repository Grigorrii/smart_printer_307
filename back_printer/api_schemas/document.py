from pydantic import BaseModel, root_validator
from typing import List, Optional
from datetime import datetime
from bson.objectid import ObjectId


class ChangeStatus(BaseModel):
    all_id: List[str]