from pydantic import BaseModel, root_validator
from bson.objectid import ObjectId
from typing import Union, Optional
from enum import Enum

from datetime import datetime


class PydanticObjectId(ObjectId):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if not isinstance(v, ObjectId):
            raise TypeError('ObjectId required')
        return str(v)


class StatusEnum(Enum):
    downloads = 0
    wait_print = 1
    error_print = 2
    successful_print = 3
    printed = 4
    deleted = 5 


class DocumntMongoDocument(BaseModel):
    id: Optional[str]
    user_id: Optional[str]
    name: Optional[str] = "undefined_doc"
    size: Optional[int]
    type: Optional[str]
    path: Optional[str]
    last_modify: Optional[int]
    time_create: Optional[datetime]
    size_print: Optional[int]
    status: Optional[StatusEnum] = 0

    class Config:
        use_enum_values = True
    
    @root_validator(pre=True)
    def fix_id(cls, values):
        if values.get("_id") is None:
            return values

        values['id'] = str(values["_id"])
        return values
