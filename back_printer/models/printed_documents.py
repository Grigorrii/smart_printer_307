from pydantic import BaseModel, root_validator
from models.document_model import StatusEnum
from bson.objectid import ObjectId
from typing import Union, Optional
from enum import Enum

from datetime import datetime, timedelta, timezone


class PydanticObjectId(ObjectId):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if not isinstance(v, ObjectId):
            raise TypeError('ObjectId required')
        return str(v)

class PrintedDocumentModel(BaseModel):
    id: Optional[str]
    doc_id: Optional[Union[PydanticObjectId, str]]
    user_id: Optional[Union[PydanticObjectId, str]]
    date_print: Optional[datetime] = datetime.now(tz=timezone(timedelta(hours=3)))
    size: int
    status: Optional[StatusEnum] = 0

    class Config:
        use_enum_values = True

    
    @root_validator(pre=True)
    def fix_id(cls, values):
        if values.get("_id") is None:
            return values

        values['id'] = str(values["_id"])
        return values
