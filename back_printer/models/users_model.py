from pydantic import BaseModel, root_validator
from bson.objectid import ObjectId
from typing import Optional

class UserDocument(BaseModel):
    id: Optional[str] = None
    username: Optional[str] = "undefined"
    name: Optional[str] = "undefined"
    email: Optional[str] = None
    rfi_id: Optional[str] = None
    password: Optional[str] = None
    role: Optional[bool] = None 

    @root_validator(pre=True)
    def fix_id(cls, values):
        if values.get("role") is None:
            values["role"] = False

        if values.get("_id") is None:
            return values

        values['id'] = str(values["_id"])
        values['name'] = values['username']
        return values
