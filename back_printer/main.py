import uvicorn
from setting_web import app
from routers.users.manager_routers import router as RouterUsers
from routers.documents.manager_routers import router as RouterDocument
from routers.workspace.manager_routers import router as RouterWorkSpace
from routers.printer.manager_router import router as RouterPrinter
from routers.admin.manager_routers import router as RouterAdmin


app.include_router(RouterUsers)
app.include_router(RouterDocument)
app.include_router(RouterWorkSpace)
app.include_router(RouterPrinter)
app.include_router(RouterAdmin)


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=5000)
