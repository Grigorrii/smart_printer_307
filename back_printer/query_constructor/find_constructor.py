from pydantic import BaseModel, root_validator
from typing import List


class FindConstructor(BaseModel):
    fields: List[str]
    params: list

    @root_validator()
    def fix_load(cls, value):
        result_value: dict = {}

        fields, params = value.get("fields"), value.get("params")

        for index in range(len(fields)):
            result_value[fields[index]] = params[index]

        return result_value
