from pydantic import BaseModel, Field, root_validator
from typing import Optional


class AggregateConstructor(BaseModel):
    match: Optional[dict] = None
    group: Optional[dict] = None
    limit: Optional[int] = 1
    
    @root_validator()
    def fix_load(cls, value):

        result_value: list = []

        for key, setting in value.items():
            if setting:
                new_key = "$" + key
                result_value.append({new_key: setting})

        return {"result": result_value}



