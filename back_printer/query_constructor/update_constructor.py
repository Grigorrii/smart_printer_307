from pydantic import BaseModel, Field, root_validator
from typing import Optional

class UpdateConstructor(BaseModel):
    set: Optional[dict] = None
    match: Optional[dict] = None
    group: Optional[dict] = None
    
    @root_validator()
    def fix_load(cls, value):

        result_value: dict = {}

        for key, setting in value.items():
            if setting:
                result_value["$" + key] = setting

        return result_value



