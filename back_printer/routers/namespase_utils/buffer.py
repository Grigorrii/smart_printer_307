from setting_web import head_conf

async def buffer_connect_user(user_id: str, connect_user_id, time_ttl: int) -> str:
    # записываем в хеш таблицу буферные данные
    head_conf.red.set(user_id, connect_user_id, time_ttl)


async def buffer_get_user(user_id: str):
    try:
        return head_conf.red.get(user_id)
    except AttributeError:
        raise AttributeError
