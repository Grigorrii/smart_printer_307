from pydantic import BaseModel, root_validator
from datetime import datetime

from datetime import datetime, timezone, timedelta
from enum import Enum
from typing import List, Union, Optional
from bson.objectid import ObjectId


class StatusEnum(Enum):
    dounloads = 0
    wait_print = 1
    error_print = 2
    successful_print = 3
    

class DocumentsCreateSchema(BaseModel):
    name_document: str
    type_document: str

    time_create: Optional[datetime] = datetime.now(tz=timezone(timedelta(hours=3)))
    status: Optional[StatusEnum] = 0

    class Config:
        use_enum_values = True
    
    @root_validator(pre=True)
    def start_create(cls, values):
        values["time_create"] = datetime.now(tz=timezone(timedelta(hours=3)))
        return values


class DocumentsChangeStatus(BaseModel):
    id_documents: List[str]

    