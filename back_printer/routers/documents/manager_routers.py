from fastapi import APIRouter, Depends, HTTPException, UploadFile, File
from fastapi.responses import JSONResponse, StreamingResponse
from fastapi import Response, Header
from datetime import datetime, timezone, timedelta
from bson.objectid import ObjectId
import os, json, shutil

from setting_web import document_collection, head_conf

from api_schemas.document import ChangeStatus
from models.document_model import DocumntMongoDocument, StatusEnum
from .utils.counter_pages import counter_pages

from users_session.setting_token import check_user, ParsToken
from users_session.utils_pass import get_hashed


router = APIRouter(prefix="/files", tags=["Files"])


@router.post("/")
async def upload_new_file(last_modify: int, data_documents: UploadFile = File(...),
                          Authorize: ParsToken = Depends(check_user)):

    # Формируем путь до файла
    pathNewFile = f"{head_conf.file_name}/files/{str(Authorize.user_id)}/{data_documents.filename}"

    while True:
        try:
            with open(pathNewFile, "wb") as buffer:
                shutil.copyfileobj(data_documents.file, buffer)
            break
        except FileNotFoundError:
            os.mkdir(os.path.join(head_conf.file_name + "/files", str(Authorize.user_id)))

    size_page = await counter_pages(data_documents.file)
    new_obj_document = DocumntMongoDocument(user_id=Authorize.user_id, path=pathNewFile, name=data_documents.filename,
                                            type=data_documents.content_type, size=size_page, last_modify=last_modify,
                                            time_create=datetime.now(tz=timezone(timedelta(hours=3))))

    doc_mongo = document_collection.insert_one(
        json.loads(new_obj_document.json()))
    new_obj_document.id = str(doc_mongo.inserted_id)

    return JSONResponse(status_code=201, content=json.loads(new_obj_document.json(exclude={'_id'})))

@router.delete("/delete_queue/")
async def drop_file(document_id: str, Authorize: ParsToken = Depends(check_user)):
    document_collection.update_one({'_id': ObjectId(document_id)}, {
                                    "$set": {"status": StatusEnum.downloads.value}})

    return JSONResponse(status_code=200, content={"message": "change_status"})


@router.post("/add_queue/")
async def send_to_print(all_id: ChangeStatus, Authorize: ParsToken = Depends(check_user)):
    document_collection.update_many({'_id': {"$in": list(map(lambda x: ObjectId(x), all_id.all_id))}}, {
                                    "$set": {"status": StatusEnum.wait_print.value}})

    return JSONResponse(status_code=200, content={"message": "change_status"})

@router.delete("/delete/")
async def drop_file(document_id: str, Authorize: ParsToken = Depends(check_user)):
    try:
        doc_info: DocumntMongoDocument = DocumntMongoDocument(
            **document_collection.find_one_and_update({"_id": ObjectId(document_id)}, {"$set": {"status": StatusEnum.deleted.value}}))
        os.remove(doc_info.path)

        return JSONResponse(status_code=200, content={"message": "good delete"})
    except:
        return JSONResponse(status_code=400, content={"message": "bad delete"})

