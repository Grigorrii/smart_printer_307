from fastapi import APIRouter, HTTPException
from fastapi.responses import StreamingResponse, JSONResponse
from fastapi import Response
from bson.objectid import ObjectId
from io import BytesIO
import codecs
from json import loads

from typing import List

from models.document_model import DocumntMongoDocument, StatusEnum
from models.users_model import UserDocument
from models.printed_documents import PrintedDocumentModel

from setting_web import document_collection, printed_doc_collection, users_collection, head_conf
from query_constructor.update_constructor import UpdateConstructor
from .utils.createPrinted import create_match
from api_schemas.document_for_print import SendForPrintSchema
from .utils.readorPrinted import reador_print


router = APIRouter(prefix="/printer", tags=["Printer"])


@router.get("/print/{rf_id}", response_model=List[SendForPrintSchema])
def print_event(rf_id: str):
    try:
        find_users: UserDocument = UserDocument(
            **users_collection.find_one({"rfi_id": rf_id.replace(" ", '')}))
    except TypeError:
        raise HTTPException(status_code=404, detail="user_not_found")

    docForPrint = list(map(lambda x: reador_print(DocumntMongoDocument(
        **x)), document_collection.find({"user_id": find_users.id, "status": StatusEnum.wait_print.value})))

    return StreamingResponse(BytesIO(str(docForPrint).encode()), media_type="application/pdf")


@router.post("/status_print/{type:int}")
async def change_status(type: StatusEnum, documnets_id: List[str]):
    documnets_ids_obj = list(map(lambda x: ObjectId(x), documnets_id))

    try:
        printed_doc_collection.update_one({{"doc_id": {"$in": documnets_id}}, {
                                          "$set": {"status": type}}, {"upsert": True}})
        document_collection.update_one(
            {{"_id": {"$in": documnets_ids_obj}}, {"$set": {"status": type}}})
    except Exception:
        raise HTTPException(status_code=404, detail="error")

    return Response(200, content="change_good")