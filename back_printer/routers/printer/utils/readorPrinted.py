from models.document_model import DocumntMongoDocument, StatusEnum
from api_schemas.document_for_print import SendForPrintSchema
from setting_web import printed_doc_collection, document_collection
from .createPrinted import create_match
from json import loads
import codecs


def reador_print(one_doc: DocumntMongoDocument) -> list:
    printed = printed_doc_collection.insert_one(create_match(one_doc))
    return codecs.open(f"{one_doc.path}", 'rb').read(), loads(SendForPrintSchema(name=one_doc.name, id=str(printed.inserted_id)).json())
