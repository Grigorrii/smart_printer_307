from models.printed_documents import PrintedDocumentModel
from models.document_model import DocumntMongoDocument
from models.document_model import StatusEnum
from setting_web import document_collection

from bson.objectid import ObjectId

from json import loads


def create_match(one_doc: DocumntMongoDocument) -> dict:
    document_collection.update_one({"_id": ObjectId(one_doc.id)}, {
                                   "$set": {"status": StatusEnum.downloads.value}})

    return loads(PrintedDocumentModel(doc_id=one_doc.id,
                                      user_id=one_doc.user_id,
                                      size=one_doc.size,
                                      status=StatusEnum.successful_print.value).json())
