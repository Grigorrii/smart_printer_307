from setting_web import users_collection, document_collection
from models.users_model import UserDocument
from models.document_model import DocumntMongoDocument

from bson.errors import InvalidId

from bson.objectid import ObjectId

def find_user(user_id: str) -> UserDocument:
    try:
        return UserDocument(**users_collection.find_one({"_id": ObjectId(user_id)}))
    except TypeError:
        return UserDocument()
    except InvalidId:
        return UserDocument()


def find_document(doc_id: str) -> DocumntMongoDocument:
    try:
        return DocumntMongoDocument(**document_collection.find_one({"_id": ObjectId(doc_id)}))
    except TypeError:
        return DocumntMongoDocument(id=doc_id)
    except InvalidId:
        return DocumntMongoDocument(id=doc_id)