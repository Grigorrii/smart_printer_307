from pydantic import BaseModel, root_validator, validator
from typing import Optional

from users_session.utils_pass import get_hashed

class AdminGroupParams(BaseModel):
    # _id = Optional[str]
    pivot_param: Optional[str] = None
    user_id: Optional[str] = None
    count_document: Optional[dict] = None
    size_pages: Optional[dict] = None
    dt_last_print: Optional[dict] = None
    size: Optional[dict]

    # @root_validator(pre=True)
    # def fix_load(cls, value):

    #     result_value: dict = {}

    #     for key, setting in value.items():
    #         if setting:
    #             result_value[key] = setting
    #     return result_value


    @root_validator()
    def fix_dump(cls, value):
        result_value: dict = {}

        for key, setting in value.items():
            if key == "pivot_param":
                result_value["_id"] = setting
                continue

            if setting:
                result_value[key] = setting
        return result_value
        

class AdminRegistration(BaseModel):
    username: str
    email: str
    rfi_id: Optional[str] = None
    password: str
    role: Optional[bool] = True


    @validator("rfi_id", allow_reuse=True)
    def formated_rf_id(cls,v):        
        return v.replace(" ", '')

    @validator("password", allow_reuse=True)
    def formated_rf_id(cls,v):
        return get_hashed(v)


class ConnctRFID(BaseModel):
    id: str
    rfi_id: str



