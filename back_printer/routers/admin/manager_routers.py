from fastapi import APIRouter, Depends, HTTPException, Response
from fastapi.responses import JSONResponse
from datetime import datetime
from json import loads
from pymongo.errors import DuplicateKeyError

from users_session.setting_token import check_user, ParsToken
from users_session.utils_pass import create_token

from query_constructor.aggregate_constructor import AggregateConstructor
from query_constructor.find_constructor import FindConstructor
from api_schemas.admins_panel import StatPeriodAllUsers, ResultOneUser
from .admin_schemas import AdminRegistration, ConnctRFID
from api_schemas.authorization import AuthorizationSchema

from .admin_schemas import AdminGroupParams

from setting_web import printed_doc_collection, users_collection
from datetime import datetime
from bson.objectid import ObjectId
from bson.errors import InvalidId

from .utils.find_user import find_user, find_document
from models.document_model import StatusEnum
from models.users_model import UserDocument

from routers.namespase_utils.buffer import buffer_connect_user, buffer_get_user

from typing import List


router = APIRouter(prefix="/admin", tags=["Admin"])


@router.get("/", response_model=List[StatPeriodAllUsers])
async def all_users(dateFrom: datetime, dateTo: datetime, Authorize: ParsToken = Depends(check_user)):
    if not Authorize.admin:
        raise HTTPException(status_code=403, detail="You not admin")

    find_con = loads(FindConstructor(fields=["date_print", "status"], params=[
                     {"$lte": dateTo, "$gte": dateFrom}, StatusEnum.printed.value]).json())

    group_con = loads(AdminGroupParams(pivot_param="$user_id", count_document={"$count": {
    }}, size_pages={"$sum": "$size"}, dt_last_print={"$last": "$date_print"}).json())

    agregate_con = loads(AggregateConstructor(
        match=find_con, group=group_con).json())
    query = printed_doc_collection.aggregate(agregate_con["result"])

    result_query = list(map(lambda x: loads(StatPeriodAllUsers(
        name=find_user(x["_id"]).username, **x).json()), query))

    return JSONResponse(status_code=200, content=result_query)


@router.get("/profile/documents/",  response_model=ResultOneUser)
async def one_user_documents(user_id: str, dateFrom: datetime, dateTo: datetime, Authorize: ParsToken = Depends(check_user)):
    if not Authorize.admin:
        raise HTTPException(status_code=403, detail="You not admin")

    # Агрегация данных по одному пользователю и дате
    find_con = loads(FindConstructor(fields=["date_print", "status", "user_id"], params=[
                     {"$lte": dateTo, "$gte": dateFrom}, StatusEnum.printed.value, user_id]).json())

    group_con = loads(AdminGroupParams(pivot_param="$user_id", count_document={"$count": {
    }}, size_pages={"$sum": "$size"}, dt_last_print={"$last": "$date_print"}).json())

    agregate_con = loads(AggregateConstructor(
        match=find_con, group=group_con, limit=1).json())
    query = printed_doc_collection.aggregate(agregate_con["result"])

    try:
        result_query = list(map(lambda x: StatPeriodAllUsers(
            name=find_user(x["_id"]).username, **x), query))[0]

        find_con = loads(FindConstructor(fields=["date_print", "user_id"], params=[
            {"$lte": dateTo, "$gte": dateFrom}, user_id]).json())

        query = printed_doc_collection.find(find_con)
        result_query_doc = list(
            map(lambda x: find_document(x["doc_id"]), query))

    except IndexError:
        result_query = StatPeriodAllUsers(id=user_id)
        result_query_doc = []

    return JSONResponse(status_code=200, content=loads(ResultOneUser(info=result_query, table=result_query_doc).json()))


@router.post("/register", response_model=AuthorizationSchema)
async def create_users(data_user: AdminRegistration):
    try:
        admin = users_collection.insert_one(loads(data_user.json()))
    except DuplicateKeyError:
        raise HTTPException(status_code=400, detail="this user exits")

    get_tocken: AuthorizationSchema = await create_token(UserDocument(id=str(admin.inserted_id), rfi_id=data_user.rfi_id, role=True))
    return JSONResponse(status_code=200,  content=loads(get_tocken.json()))


@router.get("/users/not_inactive", response_model=List[UserDocument])
async def all_users_not_connect_rf_id(Authorize: ParsToken = Depends(check_user)):
    if not Authorize.admin:
        raise HTTPException(status_code=403, detail="You not admin")

    docUser = list(map(lambda x: loads(UserDocument(
        **x).json(exclude={"password", 'username'})), users_collection.find({"rfi_id": None})))
    return JSONResponse(status_code=200,  content=docUser)


@router.get("/users/start_connect")
async def all_users_not_connect_rf_id(connect_id: str,  Authorize: ParsToken = Depends(check_user)):
    if not Authorize.admin:
        raise HTTPException(status_code=403, detail="You not admin")

    await buffer_connect_user(Authorize.user_id, connect_id, 60)
    return JSONResponse(status_code=200,  content="good")


@router.get("/users/finally_connect")
async def all_users_not_connect_rf_id(rf_id: str,  Authorize: ParsToken = Depends(check_user)):
    if not Authorize.admin:
        raise HTTPException(status_code=403, detail="You not admin")

    connect_id = await buffer_get_user(Authorize.user_id)
    
    if connect_id is None:
        raise HTTPException(status_code=404, detail="not find connect")

    try:
        user_doc = UserDocument(**users_collection.find_one_and_update(
            {"_id": ObjectId(connect_id)}, {"$set": {"rfi_id": rf_id.replace(" ", '')}}))
    except TypeError:
        raise HTTPException(status_code=400, detail='not find user')
    except InvalidId:
        raise HTTPException(status_code=400, detail='not correct id')

    return JSONResponse(status_code=200,  content=loads(user_doc.json(exclude={"password"})))
